public class manipulasiString1 {
    public static void main(String[] args) {
        String[] sd1 = {"12345", "67845"};
        findDifferentCharacters(sd1);
    }

    private static void findDifferentCharacters(String[] strings) {
        if (strings.length != 2) {
            System.out.println("Harap masukkan dua string untuk dibandingkan.");
            return;
        }

        String firstString = strings[0];
        String secondString = strings[1];

        int minLength = Math.min(firstString.length(), secondString.length());
        int differentCount = 0;
        StringBuilder differentIndices = new StringBuilder("Yaitu pada indeks: ");

        for (int i = 0; i < minLength; i++) {
            if (firstString.charAt(i) != secondString.charAt(i)) {
                differentIndices.append(i).append(", ");
                differentCount++;
            }
        }

        if (differentCount > 0) {
            differentIndices.setLength(differentIndices.length() - 2); 
            System.out.println("Total karakter tidak sama adalah " + differentCount + ". " + differentIndices.toString());
        } else {
            System.out.println("Semua karakter sama");
        }
    }
}
