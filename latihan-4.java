public class manipulasiString2 {
    public static void main(String[] args) {
        String[] supportedPhones = {"1,2,3,4,5", "6,7,8,4,5"};
        findCommonCharacters(supportedPhones);
    }

    private static void findCommonCharacters(String[] strings) {
        if (strings.length != 2) {
            System.out.println("Harap masukkan dua string untuk dibandingkan.");
            return;
        }

        String[] firstStringArray = strings[0].split(",");
        String[] secondStringArray = strings[1].split(",");

        StringBuilder commonCharacters = new StringBuilder("Data yang sama = ");

        for (String num : firstStringArray) {
            if (contains(secondStringArray, num)) {
                commonCharacters.append(num).append(",");
            }
        }

        if (commonCharacters.length() > 15) { // Cek apakah ada karakter yang sama
            commonCharacters.setLength(commonCharacters.length() - 1);  // Menghapus koma terakhir
            System.out.println(commonCharacters.toString());
        } else {
            System.out.println("Tidak ada karakter yang sama diantara kedua string.");
        }
    }

    private static boolean contains(String[] array, String value) {
        for (String str : array) {
            if (str.equals(value)) {
                return true;
            }
        }
        return false;
    }
}
