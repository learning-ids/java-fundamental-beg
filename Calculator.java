import java.util.Scanner;

public class Calculator {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        while (true) {
            System.out.println("\n--- Kalkulator Penghitung ---");
            System.out.println("Menu:");
            System.out.println("1. Hitung Luas Bidang");
            System.out.println("2. Hitung Volume");
            System.out.println("0. Exit");
            System.out.print("Masukkan pilihan (0/1/2): ");
            
            int choice = input.nextInt();
            
            switch (choice) {
                case 0:
                    System.out.println("Terima kasih! Aplikasi ditutup.");
                    input.close();
                    System.exit(0);
                    break;
                case 1:
                    hitungLuas();
                    break;
                case 2:
                    hitungVolume();
                    break;
                default:
                    System.out.println("Pilihan tidak valid. Silakan pilih 0, 1, atau 2.");
                    break;
            }
        }
    }

    public static void hitungLuas() {
        Scanner input = new Scanner(System.in);
        
        while (true) {
            System.out.println("\n-- Pilih Bidang yang Akan Dihitung Luasnya --");
            System.out.println("1. Persegi");
            System.out.println("2. Lingkaran");
            System.out.println("3. Segitiga");
            System.out.println("4. Persegi Panjang");
            System.out.println("0. Kembali ke Menu Sebelumnya");
            System.out.print("Masukkan pilihan (0-4): ");
            
            int choice = input.nextInt();
            double luas;
            
            switch (choice) {
                case 0:
                    return;
                case 1:
                    System.out.print("Masukkan sisi persegi: ");
                    double sisi = input.nextDouble();
                    luas = sisi * sisi;
                    System.out.println("Luas Persegi adalah: " + luas);
                    break;
                case 2:
                    System.out.print("Masukkan jari-jari lingkaran: ");
                    double jariJari = input.nextDouble();
                    luas = 3.14 * jariJari * jariJari;
                    System.out.println("Luas Lingkaran adalah: " + luas);
                    break;
                case 3:
                    System.out.print("Masukkan alas segitiga: ");
                    double alas = input.nextDouble();
                    System.out.print("Masukkan tinggi segitiga: ");
                    double tinggi = input.nextDouble();
                    luas = 0.5 * alas * tinggi;
                    System.out.println("Luas Segitiga adalah: " + luas);
                    break;
                case 4:
                    System.out.print("Masukkan panjang persegi panjang: ");
                    double panjang = input.nextDouble();
                    System.out.print("Masukkan lebar persegi panjang: ");
                    double lebar = input.nextDouble();
                    luas = panjang * lebar;
                    System.out.println("Luas Persegi Panjang adalah: " + luas);
                    break;
                default:
                    System.out.println("Pilihan tidak valid. Silakan pilih 0-4.");
                    break;
            }
        }
    }

    public static void hitungVolume() {
        Scanner input = new Scanner(System.in);
        
        while (true) {
            System.out.println("\n-- Pilih Bangun Ruang yang Akan Dihitung Volumenya --");
            System.out.println("1. Kubus");
            System.out.println("2. Balok");
            System.out.println("3. Tabung");
            System.out.println("0. Kembali ke Menu Sebelumnya");
            System.out.print("Masukkan pilihan (0-3): ");
            
            int choice = input.nextInt();
            double volume;
            
            switch (choice) {
                case 0:
                    return;
                case 1:
                    System.out.print("Masukkan sisi kubus: ");
                    double sisi = input.nextDouble();
                    volume = Math.pow(sisi, 3);
                    System.out.println("Volume Kubus adalah: " + volume);
                    break;
                case 2:
                    System.out.print("Masukkan panjang balok: ");
                    double panjang = input.nextDouble();
                    System.out.print("Masukkan lebar balok: ");
                    double lebar = input.nextDouble();
                    System.out.print("Masukkan tinggi balok: ");
                    double tinggi = input.nextDouble();
                    volume = panjang * lebar * tinggi;
                    System.out.println("Volume Balok adalah: " + volume);
                    break;
                case 3:
                    System.out.print("Masukkan jari-jari tabung: ");
                    double jariJari = input.nextDouble();
                    System.out.print("Masukkan tinggi tabung: ");
                    double tinggiTabung = input.nextDouble();
                    volume = 3.14 * tinggiTabung * jariJari * jariJari;
                    System.out.println("Volume Tabung adalah: " + volume);
                    break;
                default:
                    System.out.println("Pilihan tidak valid. Silakan pilih 0-3.");
                    break;
            }
        }
    }
}
